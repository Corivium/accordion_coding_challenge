    const accordions = document.querySelectorAll('.c-accordion--btn');

    //Loop through all accordion headers and add event click to each
    for(let i = 0; i < accordions.length; i++) {
        accordions[i].addEventListener('click', (e) => {
            let target = e.target; //store target element to variable
            target.classList.toggle('open'); //add class to clicked button
            let accContent = e.target.nextElementSibling; //get next sibling element (e.g. content)
            accContent.classList.toggle('active'); //toggle active class on content

            if (accContent.style.maxHeight || !e.target) {
                accContent.style.maxHeight = null;
            } else {
                accContent.style.maxHeight = accContent.scrollHeight + 'px';
            }

            // Toggle aria-expand when clicked
            let ariaExpand = e.target.getAttribute('aria-expanded');
            if (ariaExpand === true){
                e.target.setAttribute('aria-expanded', false);
            } else {
                e.target.setAttribute('aria-expanded', true);
            }

        });

        // Add event listener for keyboard usage
        accordions[i].addEventListener('keydown', (e) => {

            let keyTarget = e.target;
            let key = e.which.toString();

            let isExpanded = keyTarget.getAttribute('aria-expanded') == 'true';

            // Key numbers for up and down buttons
            let keyModifier = (event.keyControl && key.match(/33|34/));
            //console.log(keyModifier);g

            if (target.classList.contains('.c-accordion--btn')) {
                // Up/ Down arrow and Control + Page Up/ Page Down keyboard operations
                // 38 = Up, 40 = Down
                if (key.match(/38|40/) || ctrlModifier) {
                var index = triggers.indexOf(target);
                var direction = (key.match(/34|40/)) ? 1 : -1;
                var length = triggers.length;
                var newIndex = (index + length + direction) % length;

                triggers[newIndex].focus();

                event.preventDefault();
                }
                else if (key.match(/35|36/)) {
                // 35 = End, 36 = Home keyboard operations
                switch (key) {
                    // Go to first accordion
                    case '36':
                    triggers[0].focus();
                    break;
                    // Go to last accordion
                    case '35':
                    triggers[triggers.length - 1].focus();
                    break;
                }
                event.preventDefault();

                }

            }
        });

        // Apply focus class to buttons when tabbing
        accordions.forEach(function (trigger) {

            trigger.addEventListener('focus', function (event) {
                this.classList.add('focus');
            });

            trigger.addEventListener('blur', function (event) {
                this.classList.remove('focus');
            });

        });



    }

