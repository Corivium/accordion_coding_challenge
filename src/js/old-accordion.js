// Global Javascript

    //Get header of accordion
    const accordionToggle = document.querySelectorAll('.c-accordion__panel--header');

    //Loop through all accordion headers and add event click to each
    for(let i = 0; i < accordionToggle.length; i++) {
        accordionToggle[i].addEventListener('click', (e) => {
            e.target.classList.toggle('open'); //Add class to header


            // Toggle aria-expand
            let ariaExpand = e.target.getAttribute('aria-expanded');
            if (ariaExpand === true){
                e.target.setAttribute('aria-expanded', false);
            } else {
                e.target.setAttribute('aria-expanded', true);
            }

            //Activate Slide Toggle
            slideToggle(e.target.nextElementSibling, 300);

        });
    } 


    //Add slidetoggle functionality

    //Slide-up
    let slideUp = (target, duration) => {
        target.style.transitionProperty = 'height, margin, padding';
        target.style.transitionDuration = duration + 'ms'; 
        target.style.boxSizing = 'border-box'; 
        target.style.height = target.offsetHeight + 'px'; 
        target.style.height = 0; 
        target.style.paddingTop = 0; 
        target.style.paddingBottom = 0;
        target.style.marginTop = 0; 
        target.style.marginBottom = 0; 
        target.style.overflow = 'hidden';

        //Reset all properties after animation completes
        window.setTimeout( () => {
            target.style.display = 'none';
            target.style.removeProperty('height'); 
            target.style.removeProperty('padding-top');  
            target.style.removeProperty('padding-bottom'); 
            target.style.removeProperty('margin-top'); 
            target.style.removeProperty('margin-bottom'); 
            target.style.removeProperty('overflow'); 
            target.style.removeProperty('transition-duration'); 
            target.style.removeProperty('transition-property'); 
        }, duration);
    }

    let slideDown = (target, duration) => {
        target.style.removeProperty('display'); 
        let display = window.getComputedStyle(target).display;
        if (display === 'none') {
            display = 'block';
        }
        target.style.display = display;

        let elHeight = target.offsetHeight; 
        target.style.height = 0; 
        target.style.paddingTop = 0;
        target.style.paddingBottom = 0; 
        target.style.marginTop = 0; 
        target.style.marginBottom = 0; 
        target.style.overflow = 'hidden'; 

        //slide down logic
        target.style.boxSizing = 'border-box';
        target.style.transitionProperty = "height, margin, padding"; 
        target.style.transitionDuration = duration + 'ms'; 
        target.style.height = elHeight + 'px'; 
        target.style.removeProperty('padding-top'); 
        target.style.removeProperty('padding-bottom');  
        target.style.removeProperty('margin-top'); 
        target.style.removeProperty('margin-bottom'); 

        //Reset properties when animation completes
        window.setTimeout( () => {
            target.style.removeProperty('height'); /* [13] */
            target.style.removeProperty('overflow'); /* [14] */
            target.style.removeProperty('transition-duration'); /* [15.1] */
            target.style.removeProperty('transition-property'); /* [15.2] */
        }, duration);

    }

    //Set up a toggle function with target and duration perameters
    let slideToggle = (target, duration = 500) => {
        if (window.getComputedStyle(target).display === 'none') {
            return slideDown(target, duration);
        } else {
            return slideUp(target, duration);
        }
    }

    //console.log('accordion initialized');

